FROM python:alpine3.11 as builder

COPY qemu-*-static /usr/bin/

FROM builder

ARG VERSION=1.4.0

LABEL maintainer="Jay MOULIN <jaymoulin@gmail.com> <https://twitter.com/MoulinJay>"
LABEL version=${VERSION}

RUN apk update && \
    apk add linux-headers g++ libxslt-dev libxml2-dev --no-cache --virtual .build-deps && \
    mkdir /root/oauth && \
    pip3 install --upgrade pip && \
    pip3 install google-music-manager-downloader && \
    apk del g++ --purge .build-deps

ADD ./daemon.sh /root/daemon.sh
ADD ./auth.sh /root/auth

VOLUME /media/library
VOLUME /root/oauth

WORKDIR /root
ENV PATH="/root:{$PATH}"
CMD ["/root/daemon.sh"]
